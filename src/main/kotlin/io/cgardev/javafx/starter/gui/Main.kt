package io.cgardev.javafx.starter.gui

import javafx.application.Application
import javafx.fxml.FXMLLoader
import javafx.scene.Parent
import javafx.scene.Scene
import javafx.stage.Stage

class Main : Application(){
    @Throws(Exception::class)
    override fun start(primaryStage: Stage) {

        val root: Parent = FXMLLoader.load(javaClass.getResource("/mainWindow.fxml")!!)
        val scene = Scene(root)

        primaryStage.title = "JavaFX Starter GUI"
        primaryStage.scene = scene
        primaryStage.minWidth = 500.0
        primaryStage.minHeight = 500.0

        primaryStage.show()
    }
}

fun main(args: Array<String>) {
    Application.launch(Main::class.java, *args)
}