package io.cgardev.javafx.starter.gui.controller

import javafx.fxml.FXML
import javafx.fxml.Initializable
import javafx.scene.control.Alert
import javafx.scene.control.Button
import javafx.scene.control.CheckBox
import javafx.scene.control.TextField
import java.net.URL
import java.util.*

class MainWindowCtrl : Initializable{


    @FXML private var locationBtn = Button()
    @FXML private var locationInput = TextField()
    @FXML private var nameInput = TextField()

    @FXML private var uiKitCBox = CheckBox()
    @FXML private var uiVuetifyCBox = CheckBox()

    @FXML private var createBtn = Button()

    override fun initialize(location: URL?, resources: ResourceBundle?) {



        createBtn.setOnAction {
            if(isValid()){
                nameInput.text = isValid().toString()
            }else{
                nameInput.text = isValid().toString()
                errorAlert("There are empty fields.")
            }
        }
    }

    private fun errorAlert(content: String, header: String = "Validation error"){
        val alert = Alert(Alert.AlertType.ERROR)
        alert.title = "Error"
        alert.headerText = header
        alert.contentText = content
        alert.showAndWait()
    }

    private fun isValid(): Boolean{
        if(locationInput.text.isEmpty() || nameInput.text.isEmpty()){
            return false
        }
        return true
    }




}