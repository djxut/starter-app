package io.cgardev.javafx.starter.core

import java.nio.file.Paths

enum class UiLibrary() {
 UIKIT, VUETIFY
}

enum class Database(){
    POSTGRESQL, MONGODB
}

class Djuxt(var location: String, var name: String, var uiLibraries: List<UiLibrary>,
            var databases: List<Database>, var forceSSL: Boolean){

    private constructor(builder: Builder) :
            this(builder.location, builder.name, builder.uiLibraries,
                    builder.databases, builder.forceSSL)

    class Builder {
        var location = ""
            private set
        var name = ""
            private set
        var uiLibraries = ArrayList<UiLibrary>()
            private set
        var databases = ArrayList<Database>()
            private set
        var forceSSL = false
            private set


        fun build() = Djuxt(this)


    }
}

class DjuxtBuilder{

    var location = ""
        private set
    var name = ""
        private set
    var uiLibraries = ArrayList<UiLibrary>()
        private set
    var databases = ArrayList<Database>()
        private set
    var forceSSL = false
        private set

    init {
        databases.add(Database.POSTGRESQL)
    }

    fun addUiLibrary(library: UiLibrary): DjuxtBuilder {
        if(!this.uiLibraries.contains(library)){
            this.uiLibraries.add(library)
        }

        return this
    }
}

class Generator(var options: Djuxt) {


    private fun createProjectFolder(): Boolean{
        return Paths.get(options.location, options.name).toFile().mkdir()
    }

    fun generate(){
        createProjectFolder()
    }


}